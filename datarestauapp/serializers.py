from .models import Restau,Menu,Produit
from rest_framework import serializers

class RestauSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restau
        #fields = ('Nom_Restau', 'Ville_Restau', 'Adresse_Restau', 'last_name', 'email')
        fields = '__all__'

class MenuSerializer(serializers.ModelSerializer):
    #idRestau = serializers.IntegerField(source='Restau.id')
    def to_representation(self, instance):
        self.fields['produit'] =  ProduitSerializer(read_only=True)
        self.fields['restau'] =  RestauSerializer(read_only=True)
        return super(MenuSerializer, self).to_representation(instance)

    class Meta:
        model = Menu
        #fields = ('idRestau',)
        fields = '__all__'

class ProduitSerializer(serializers.ModelSerializer):
    #libele = serializers.StringRelatedField(source='Produit.name', many=False)
    class Meta:
        model = Produit
        #fields = ('idRestau',)
        fields = '__all__'

