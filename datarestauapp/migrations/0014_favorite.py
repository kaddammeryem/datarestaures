# Generated by Django 2.2.7 on 2020-09-23 21:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datarestauapp', '0013_remove_restau_favorite'),
    ]

    operations = [
        migrations.CreateModel(
            name='Favorite',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ID_Restau', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datarestauapp.Restau')),
                ('Id_Client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datarestauapp.Client')),
            ],
        ),
    ]
