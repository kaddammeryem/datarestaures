# Generated by Django 3.1.1 on 2020-09-17 18:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datarestauapp', '0008_restau_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restau',
            name='Image',
        ),
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Image_Restau', models.FileField(null=True, upload_to='')),
                ('Id_Restau', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datarestauapp.restau')),
            ],
        ),
    ]
