# Generated by Django 2.2.7 on 2020-11-30 20:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datarestauapp', '0021_auto_20201130_1935'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='Picture',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
