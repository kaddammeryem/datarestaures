from django.apps import AppConfig


class DatarestauappConfig(AppConfig):
    name = 'datarestauapp'
