
from django.urls import path
from . import views,menuView

urlpatterns = [
    path('reservation',views.receiveReservation),
    path('clientFacebook',views.saveClientFacebook),
    path('clientGoogle',views.saveClientGoogle),
    path('reservations',views.sendReservations),
    path('search',views.receiveSearch),
    path('favorite',views.receiveFavorite),
    path('clientfavorite/<str:idClient>',views.sendFavorite),
    path('rating',views.receiveRating),
    path('filter',views.receiveFilter),
    path('restaus/<int:pk>', views.RestauDetail.as_view()),
    path('restaus/<str:field>/<int:idRestau>/<int:rang>', views.RestauDetail.as_view()),
    path('restaus/', views.RestauDetail.as_view()),
    path('restaus/<str:field>/<str:value>/', views.RestauDetail.as_view()),
    path('menus/<int:pk>', menuView.MenuDetail.as_view()),
    path('menus/', menuView.MenuDetail.as_view()),
    path('menus/<str:field>/<str:value>/', menuView.MenuDetail.as_view()),



]
