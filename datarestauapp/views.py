from django.http import JsonResponse,HttpResponse,Http404
from datarestauapp.models import Restau,Client,Reservation,Images,Menu,Produit
from datarestauapp.serializers import RestauSerializer,ProduitSerializer
import json
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Create your views here.


class RestauDetail(APIView):

    def get_object(self, pk):
        try:
            return Restau.objects.get(pk=pk)
        except Restau.DoesNotExist:
            raise Http404

    def get(self, request, pk=None,field=None,idRestau=None,rang=None):
        #user = self.get_object(pk)
        #user = restauSerializer(user)
        if field=='image' :
            path_tofile='/home/kaddammeryem/datarestau/datarestauapp/images/'+str(idRestau)+'_'+str(rang)+'.bmp'
            img = (open(path_tofile,'rb')).read()

            return HttpResponse(img,content_type='image/bmp')
        if pk :
            restau = self.get_object(pk)
            serializer=RestauSerializer(restau)
        else :
            restaus=Restau.objects.all()
            serializer=RestauSerializer(restaus,many=True)

        return Response(serializer.data)

    def put(self, request, pk):
        restau = self.get_object(pk)
        serializer = RestauSerializer(restau, data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        restau = self.get_object(pk)
        restau.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self, request):
        serializer = RestauSerializer(data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProduitDetail(APIView):

    def get_object(self, pk):
        try:
            return Produit.objects.get(pk=pk)
        except Produit.DoesNotExist:
            raise Http404

    def get(self, request, pk=None,field=None,value=None):
        #user = self.get_object(pk)
        #user = restauSerializer(user)
        if pk :
            produit = self.get_object(pk)
            serializer=ProduitSerializer(produit)
        else :
            produits=Produit.objects.all()
            serializer=ProduitSerializer(produits,many=True)
        return Response(serializer.data)

    def put(self, request, pk):
        produit = self.get_object(pk)
        serializer = ProduitSerializer(produit, data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        produit = self.get_object(pk)
        produit.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self, request):
        serializer = ProduitSerializer(data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




def receiveRating(request):
    data=json.loads(request.body)
    restau=Restau.objects.get(id=data['idRestau'])
    restau.nbrRating=restau.nbrRating+1
    restau.save()
    restau.rating=+data['rating']/restau.nbrRating
    restau.save()
    restau2=Restau.objects.get(id=data['idRestau'])
    return JsonResponse({'rating':restau2.rating,'nbrRating':restau2.nbrRating},safe=False)

def receiveFavorite(request):
    data=json.loads(request.body)
    idClient=data['client']
    idRestau=data['restau']
    restau=Restau.objects.get(id=idRestau)
    favorite=Favorite.objects.filter(ID_Restau=restau).exists()
    client=Client.objects.get(cle_FB=idClient)
    f=open('/home/kaddammeryem/datarestau/datarestauapp/test.txt','w')
    f.write(str(favorite))
    f.close()
    if favorite==False:
        favoriteSave=Favorite()
        favoriteSave.Id_Client=client
        favoriteSave.ID_Restau=restau
        favoriteSave.save()
    else:
        Favorite.delete(Favorite.objects.get(ID_Restau=restau))

    return JsonResponse({'done':'yes'},safe=False)





def sendFavorite(request,idClient):
    client=Client.objects.all().get(cle_FB=idClient)
    favorite=Favorite.objects.all().filter(Id_Client=client)
    listFavorite=list(favorite.values())
    data=[]
    for i in range (0,len(listFavorite)):
        fav=listFavorite[i]
        idRestau=fav.get('ID_Restau_id')
        restau=Restau.objects.get(id=idRestau)
        name=restau.Nom_Restau
        adresse=restau.Adresse_Restau
        categorie=restau.Categorie_Restau
        data.append({'categorieRestau':categorie,'adresseRestau':adresse,'nameRestau':name,'idRestau':idRestau})

    return JsonResponse(data,safe=False)




def receiveSearch(request):
    data=json.loads(request.body)
    search=data['search']
    array=[]
    restau=Restau.objects.all()
    name=restau.get(Nom_Restau=search)
    if name!=None:
            s={'id':name.id,'nom_Restau':name.nom_Restau,'adresse_Restau':name.adresse_Restau,'categorie_Restau':name.categorie_Restau,'a_propos':name.a_propos,'facebook':name.facebook,
            'instagram':name.instagram,'telephone':name.telephone,'twitter':name.twitter}
            array.append(s)
            return JsonResponse(array,safe=False)


def receiveFilter(request):
    data=json.loads(request.body)
    category=data['category']
    city=data['city']
    restau=Restau.objects.all()
    if category=='All' and city=='All':
        return JsonResponse(list(restau.values()),safe=False)
    if category=='All':
        ville=restau.filter(ville_Restau=city)
        f=open('/home/kaddammeryem/datarestau/datarestauapp/test.txt','w')
        f.write(str(ville[0]))
        f.close()
        array=[]
        for i in range(len(ville)) :
            s={'id':ville[i].id,'nom_Restau':ville[i].nom_Restau,'adresse_Restau':ville[i].adresse_Restau,'ville_Restau:':ville[i].ville_Restau,'categorie_Restau':ville[i].categorie_Restau,'a_propos':ville[i].a_propos,'facebook':ville[i].facebook,
            'instagram':ville[i].instagram,'telephone':ville[i].telephone,'twitter':ville[i].twitter,'nbrRating':ville[i].nbrRating,'rating':ville[i].rating}
            array.append(s)
        return JsonResponse(array,safe=False)
    if city=='All':
        cat=restau.filter(Categorie_Restau=category)
        array=[]
        for i in range (len(cat)):
            s={'id':cat[i].id,'nom_Restau':cat[i].nom_Restau,'adresse_Restau':cat[i].adresse_Restau,'ville_Restau:':cat[i].ville_Restau,'categorie_Restau':cat[i].categorie_Restau,'a_propos':cat[i].a_propos,'facebook':cat[i].facebook,
            'instagram':cat[i].instagram,'telephone':cat[i].telephone,'twitter':cat[i].twitter,'nbrRating':cat[i].nbrRating,'rating':cat[i].rating}
            array.append(s)
        return JsonResponse(array,safe=False)
    name=restau.filter(Categorie_Restau=category,Ville_Restau=city)
    if name!=None:
        array=[]
        for i in range (len(name)):
            s={'id':name[i].id,'nom_Restau':name[i].nom_Restau,'adresse_Restau':name[i].adresse_Restau,'ville_Restau:':name[i].ville_Restau,'categorie_Restau':name[i].categorie_Restau,'a_propos':name[i].a_propos,'facebook':name[i].facebook,
            'instagram':name[i].instagram,'telephone':name[i].telephone,'twitter':name[i].twitter,'nbrRating':name[i].nbrRating,'rating':name[i].rating}
            array.append(s)

        return JsonResponse(array,safe=False)


def sendReservations(request):
    reservation=Reservation.objects.all()
    reservation2=list(reservation.values())
    data=[]
    for i in range (0,len(reservation2)):
        reserv=reservation2[i]
        idRestau=reservation.values_list('restau')[i][0]
        restau=Restau.objects.get(id=idRestau)
        name=restau.nom_Restau
        adresse=restau.adresse_Restau
        heure=reserv['Heure']
        date=reserv['Date']
        nbr=reserv['nombre_Personnes']
        data.append({'date':date,'time':heure,'adresse':adresse,'nbr':nbr,'nameRestau':name})

    return JsonResponse(data,safe=False)

def receiveReservation(request):
    data=json.loads(request.body)
    idClient=data['id']
    client=Client.objects.get(cle_FB=idClient)
    nameRestau=data["nameRestau"]
    restauObj=Restau.objects.get(nom_Restau=nameRestau)
    reservation=Reservation(
    client=client,
    restau=restauObj,
    nombre_Personnes=data["nbr"],
    Date=data["date"],
    Heure=data["time"])
    reservation.save()

    return JsonResponse({'name':'meryem'},safe=False)

def saveClientFacebook(request):
    token=json.loads(request.body)
    dictio={'access_token':token['token']}
    f=open('/home/kaddammeryem/datarestau/datarestauapp/test.txt','w')
    data2=requests.get(url="https://graph.facebook.com/me?fields=id,email,name,picture",params=dictio)
    content=data2.json()
    client2=Client.objects.filter(cle_FB=content['id'])
    if bool(client2) :
        f.write(str(list(client2.values())))
        f.close()
        return JsonResponse(list(client2.values()),safe=False)
    else:
        client=Client()
        idClient=content['id']
        nomPrenom=content['name'].split()
        name=nomPrenom[0]
        prenom=nomPrenom[1]
        client.cle_FB=idClient
        client.nom_Personne=name
        client.prenom_Personne=prenom
        client.mail_Personne=content['email']
        client.picture=content['picture']['data']['url']
        client.save()

        return JsonResponse({'nom_Personne':name,'prenom_Personne':prenom,'cle_FB':client.cle_FB,'mail_Personne':client.Mail_Personne,'nouveau':True,'picture':client.Picture},safe=False)#ici il faut ajouter l'id


def saveClientGoogle(request):
    token=json.loads(request.body)
    f=open('/home/kaddammeryem/datarestau/datarestauapp/test.txt','w')
    data2=requests.get(url="https://accounts.google.com/o/oauth2/v2/auth?&client_id="+
    "969809200509-0jdgche9jtdbcuu385b21dnffb4b2o5o.apps.googleusercontent.com&redirect_uri=https://auth.expo.io/@me/spartable&response_type=code&access_type=offline&scope=profil")
    f.write(str(data2))
    f.close()

    #client2=Client.objects.filter(cle=token['user']['id'])
    #if bool(client2) :
    #    return JsonResponse(list(client2.values()),safe=False)
    #else:
    #    client=Client()
    #    idClient=token['user']['id']
    #    nom=token['user']['familyName']
    ##    client.cle=idClient
    #    client.Nom_Personne=nom
    #    client.Prenom_Personne=prenom
    #    client.Mail_Personne=token['user']['email']
    #    client.save()

    #    return JsonResponse({'name':nom,'prenom':prenom,'id':client.cle,'email':client.Mail_Personne,'nouveau':True},safe=False)
    return JsonResponse({'name':"mery"})



