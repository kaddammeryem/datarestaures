from django.db import models


# Create your models here.


class Produit(models.Model):


    libele_Produit = models.CharField(max_length=50,null=True)
    prix_Produit= models.FloatField(null=True)

class Menu(models.Model):


    restau = models.ForeignKey('Restau', on_delete=models.CASCADE)
    produit= models.ForeignKey('Produit', on_delete=models.CASCADE)


class Client(models.Model):


    cle_FB=models.CharField(max_length=100,null=True)
    nom_Personne = models.CharField(max_length=50,null=True)
    prenom_Personne= models.CharField(max_length=50,null=True)
    mail_Personne = models.CharField(max_length=50,null=True)
    picture=models.CharField(max_length=500,null=True)



class Restau(models.Model):

    nom_Restau = models.CharField(max_length=50,null=True)
    ville_Restau = models.CharField(max_length=100,null=True)
    adresse_Restau = models.CharField(max_length=100,null=True)
    categorie_Restau =models.CharField(max_length=30,null=True)
    a_propos=models.TextField(null=True)
    facebook=models.URLField(null=True)
    instagram=models.URLField(null=True)
    twitter=models.URLField(null=True)
    telephone=models.CharField(null=True,max_length=30)
    rating=models.DecimalField(null=True,max_digits=1,decimal_places=1)
    nbrRating=models.IntegerField(null=True)
    latitude=models.FloatField(null=True)
    longitude=models.FloatField(null=True)


class Reservation(models.Model):

    client = models.ForeignKey('Client',on_delete=models.CASCADE)
    restau = models.ForeignKey('Restau',on_delete=models.CASCADE)
    nombre_Personnes = models.IntegerField(default=1)
    Date=models.DateField(auto_now=False)
    Heure=models.TimeField(auto_now=False)


class Images(models.Model):
    restau=models.ForeignKey('Restau',on_delete=models.CASCADE)
    image_Restau=models.FileField(null=True)

# reste le modele de contact a ajouter ulterieurement



