
from django.contrib import admin
from .models import  Client,Restau,Reservation,Menu,Produit


# Register your models here.

admin.site.register(Client)
admin.site.register(Restau)
admin.site.register(Reservation)
admin.site.register(Menu)
admin.site.register(Produit)
