from django.http import Http404
from datarestauapp.models import Menu,Restau
from datarestauapp.serializers import MenuSerializer,RestauSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status




class MenuDetail(APIView):

    def get_object(self, pk):
        try:
            return Menu.objects.get(pk=pk)
        except Menu.DoesNotExist:
            raise Http404

    def get(self, request, pk=None,field=None,value=None):
        if field=='restau' :
            restau=Restau.objects.get(id=int(value))
            serializedRestau=RestauSerializer(restau.id)
            #print('suivi : '+ serializedRestau.data.__str__())
            #print('suivi : '+ serializedRestau.__str__())
            menus=Menu.objects.filter(restau=restau.id)
            print('suivi : '+ menus.__str__())
            serializer=MenuSerializer(menus,many=True)
        elif pk :
            menu = self.get_object(pk)
            serializer=MenuSerializer(menu)
        else :
            menus=Menu.objects.all()
            serializer=MenuSerializer(menus,many=True)
        return Response(serializer.data)

    def put(self, request, pk):
        menu = self.get_object(pk)
        serializer = MenuSerializer(menu, data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        restau = self.get_object(pk)
        restau.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def post(self, request):
        serializer = MenuSerializer(data=request.DATA)
        if serializer.is_valid() :
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
